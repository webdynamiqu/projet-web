<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Créer une publication</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
    <link href="publication.css" rel="stylesheet" type="text/css">
  </head>
  <body>
   
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
		
       <a class="navbar-brand" href="#"><img src="images/Logo2.jpg" width="43.4" height="40.6" alt=""/></a><!-- 124*116 a la base -->
		
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		   
       <span class="navbar-toggler-icon"></span>
		   
       </button>
		
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
                <a class="nav-link" href="accueil.php">Accueil</a>
             </li>
			  
             <li class="nav-item">
                <a class="nav-link" href="profil.php">Profil</a>
             </li>
             <li class="nav-item">
                <a class="nav-link" href="emplois.php">Emplois</a>
             </li>
			  <li class="nav-item">
                <a class="nav-link" href="network.php">Mon Réseau</a>
             </li>
		    <li class="nav-item">
                <a class="nav-link" href="notifications.html">Notifications</a>
            </li>
			   <li class="nav-item">
                <a class="nav-link" href="connexion.html">Deconnexion</a>
            </li>
          </ul>
		  <form class="form-inline my-2 my-lg-0" action="recherche.php" method="post">
             <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="Search">
             <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="valider">Search</button>
          </form>
       </div>
    </nav>
	  
	<div class="jumbotron">
		
	  <h1 class="display-4">Nouvelle publication:</h1>
		<br/>
		<br/>
		<center>
			<form action="element.php" method="post">
			<textarea name="txtArea" class="textarea" >
				Rédiger votre publication.
			</textarea>
				
				
		  <br/>
		  <br/>
	
		  <h5>Lieu : <input type="text" name="lieu"> </h5>
		  <br/>
		  <br/>
		  <h5> Date  : <input type="date" name="date"></h5>
		  <br/>
	<!--<form action="/action_page.php">-->
  			<input type="file" name="pic" accept="image/*">
  			<!--<input type="submit">	
		</form> -->
				<hr class="my-4">
		<button class="btn btn-outline-success my-2 my-sm-0" style="width: 8%; height: 15%;" type="submit" value="valider">  Publier  </button>
		
		</form>
			
		</center>
	  <hr class="my-4">
		
  </div>
	  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
    <script src="js/jquery-3.2.1.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    <script src="js/popper.min.js"></script>
  <script src="js/bootstrap-4.0.0.js"></script>
  </body>
</html>
