
<?php 
session_start();
require 'configure.php';
	$database="jobdealeur";
	$db_handle=mysqli_connect(DB_SERVEUR,DB_USER,DB_PASS);
	$db_found=mysqli_select_db($db_handle,$database);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Créer une offre</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap-4.0.0.css" rel="stylesheet">
    <link href="publication.css" rel="stylesheet" type="text/css">
	  <link rel="stylesheet" type="text/css" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	  <link rel="stylesheet" type="text/css" href="no.css">
	  
	  <script type="text/javascript">
  
function change()
{
 document.getElementById("panel").style.display = "block";
}

function changeback()
{
 document.getElementById("panel").style.display = "none";
}

</script>

	  
  </head>
  <body>
   
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark">
		
       <a class="navbar-brand" href="#"><img src="images/Logo2.jpg" width="43.4" height="40.6" alt=""/></a><!-- 124*116 a la base -->
		
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		   
       <span class="navbar-toggler-icon"></span>
		   
       </button>
		
       <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav mr-auto">
             <li class="nav-item active">
                <a class="nav-link" href="accueil.php">Accueil</a>
             </li>
			  
             <li class="nav-item">
                <a class="nav-link" href="profil.php">Profil</a>
             </li>
             <li class="nav-item">
                <a class="nav-link" href="emplois.php">Emplois</a>
             </li>
			  <li class="nav-item">
                <a class="nav-link" href="network.php">Mon Réseau</a>
             </li>
		    <li class="nav-item">
                <a class="nav-link" href="#" onclick="change();">Notifications</a>
            </li>
			   <li class="nav-item">
                <a class="nav-link" href="connexion.html">Deconnexion</a>
            </li>
          </ul>
		  <form class="form-inline my-2 my-lg-0" action="recherche.php" method="post">
             <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" name="Search">
             <button class="btn btn-outline-success my-2 my-sm-0" type="submit" value="valider">Search</button>
          </form>
       </div>
    </nav>
	  
	<div class="jumbotron" onclick="changeback();">
		
	  <h1 class="display-4">Créer une offre d'Emploi : </h1>
		<br/>
		<br/>
		<center>
			<form action="ajouteremploi.php" method="post">
				
			<textarea name="description" class="textarea" >
				Description de l'emploi:
			</textarea>
		  <br/>
		  <br/>
			<h5>Entreprise : <input type="text" name="entreprise"> </h5>
			<br/>
		  <br/>
				<h5>Poste : <input type="text" name="poste"> </h5>
		  <br/>
		  <br/>
				<h5>Majeure : <input type="text" name="majeure"> </h5>
		  <br/>
		  <br/>

		  <h5>Salaire : <input type="text" name="salaire"> </h5>

		  <br/>
		  <br/>
				<hr class="my-4">
		<button class="btn btn-outline-success my-2 my-sm-0" style="width: 8%; height: 15%;" type="submit" value="valider">  Publier  </button>
		
		</form>
			
		</center>
	  <hr class="my-4">
		
  </div>
	  		  <div id="panel">
		<div class="title">Notifications</div>
      <ul class="notification-bar">
<?php $sql12="SELECT a.utilisateurID,u.nom,u.prenom FROM amis a,utilisateur u WHERE a.utilisateurID2='".$_SESSION['ID']."' AND u.ID=a.utilisateurID AND NOT EXISTS ( SELECT a2.utilisateurID FROM amis a2 WHERE a2.utilisateurID='".$_SESSION['ID']."' AND a2.utilisateurID2=a.utilisateurID)";
		  
		  $result12=mysqli_query($db_handle,$sql12);
		  while($data12=mysqli_fetch_assoc($result12)){?>
			   <li >
              <i class="ion-person-add"></i>
              <div><?php  echo $data12['nom']."  ".$data12['prenom']; ?> vous a demandé en amis </div>
				   <form action="suivre.php" method="post">
				   <input type="hidden" name="ajouter" value="<?php echo $data12['utilisateurID'] ?>">
					<button class="btn btn-success btn-lg" style="width: 35px; height: 35px; padding-left: 10px" type="submit" value="confirmer"><span class="ion-checkmark-round"></span></button>
					   <!-- <input type="submit" value="confirmer"> -->
				   </form>
				   
				   <form action="refuser.php" method="post">
				   <input type="hidden" name="ajouter1" value="<?php echo $data12['utilisateurID'] ?>">
					   <button class="btn btn-danger btn-lg" style="width: 35px; height: 35px; padding-left: 10px" type="submit" value="refuser"><span class="ion-close-round"></span></button>
					<!-- <input type="submit" value="refuser"> -->
				   </form>	
          </li>
			  
		 <?php }
		  ?>
  </div> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
    <script src="js/jquery-3.2.1.min.js"></script>

    <!-- Include all compiled plugins (below), or include individual files as needed --> 
    <script src="js/popper.min.js"></script>
  <script src="js/bootstrap-4.0.0.js"></script>
  </body>
</html>
